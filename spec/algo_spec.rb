require 'algo'

RSpec.shared_examples 'check_array' do |my_arr|
  context 'when looking in array' do
    let(:arr) { my_arr }

    it 'find all lost_numbers' do
      expect(subject.size).to eq missing_numbers
    end

    it 'lost_numbers + array in valid scope' do
      result_arr = (subject + arr).sort
      expect(result_arr).to eq [1, 2, 3, 4, 5]
    end

    it 'do not contain array elements in lost_numbers' do
      expect(subject).not_to include(*arr)
    end
  end
end

RSpec.describe Algo do
  let(:arr) { raise 'define me!' }
  let(:missing_numbers) { 2 }

  describe '#find_lost_numbers' do
    subject { described_class.find_lost_numbers(arr, missing_numbers) }

    include_examples 'check_array', [1, 2, 3]
    include_examples 'check_array', [3, 4, 5]
    include_examples 'check_array', [1, 3, 5]
    include_examples 'check_array', [2, 3, 5]
  end

  describe '#find_lost_numbers_ruby_style' do
    subject { described_class.find_lost_numbers_ruby_style(arr, missing_numbers) }

    include_examples 'check_array', [1, 2, 3]
    include_examples 'check_array', [3, 4, 5]
    include_examples 'check_array', [1, 3, 5]
    include_examples 'check_array', [2, 3, 5]
  end
end
