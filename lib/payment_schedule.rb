#
# https://gist.github.com/sletix/ea94b9e484a833dfd1b2b353763f71b4
#
# * Клиент может взять сумму на n дней, под определенный процент в день. 
# Клиент обязан погашать долг m раз на протяжении n дней (считаем что m кратно n). 
# Требуется написать программу для рассчета общей суммы долга, и суммы частичного погашения. 
# Основное условие, что бы во всех суммах не было копеек.
# Исходные данные: сумма для клиента, процентная ставка в день, кол-во дней, кол-во частичных погашений.
# > Пример: клиент взял 10 тысяч на 10 дней, с обязаностью погасить долг два раза, под ноль процентов. 
#.   Тогда 5 тысяч он отдаст через 5 дней, и еще 5 тысяч клиент отдаст на 10-й день.
#
class PaymentSchedule
  attr_reader :amount, :m, :n, :daily_percent

  def initialize(amount:, m:, n:, daily_percent:)
    set_attributes(amount, m, n, daily_percent)
    validate_input
  end

  def call
    puts "\nTotal debt: #{total_debt}"
    puts "\n-= Payments schedule =-"
    payments_array.each.with_index do |payment, i|
      puts "Day: #{calc_payment_day(i)} | payment: #{payment}"
    end
    puts ""
  end

  def percent
    @_percent ||= daily_percent * n
  end

  def percent_amount
    @_percent_amount ||= amount / 100 * percent
  end

  def total_debt
    @_total_debt ||= amount + percent_amount
  end

  def period_payment
    @_period_payment ||= total_debt / m
  end

  def total_remainder
    @_total_remainder ||= total_debt % m
  end

  def payments_array
    @_payments_array ||= begin
      basic_payments = Array.new(m, period_payment)

      rem = total_remainder
      basic_payments.each.with_index do |_el, i|
        break if rem <= 0
        rem -= 1
        basic_payments[i] += 1
      end

      basic_payments
    end
  end

  private

  def calc_payment_day(i)
    (i + 1) * (n / m)
  end

  def set_attributes(amount, m, n, daily_percent)
    @amount = amount
    @m = m
    @n = n
    @daily_percent = daily_percent
  end

  def validate_input
    errors = []
    erros  << '"n & m should be positive integer"' unless positive_integer?(n) || positive_integer?(m)
    errors << '"n % m != 0"' if n % m != 0
    errors << '"amount should be positive integer"' unless positive_integer?(amount)
    errors << '"invalid percent"' unless positive_integer?(daily_percent)

    if errors.count > 0
      raise ArgumentError, 'Invalid arguments: ' << errors.join(', ')
    end
  end

  def positive_integer?(num)
    num.is_a?(Integer) && num > 0
  end
end
