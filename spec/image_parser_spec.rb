require 'image_parser'

RSpec.describe ImageParser do
  let(:url) { 'wat' }
  subject { described_class.new.call(url: url) }

  context 'with invalid url' do
    before do
      allow_any_instance_of(described_class).to receive(:make_a_request)
        .and_raise(Errno::ECONNREFUSED)
    end

    it 'sends a corresponding message' do
      is_expected.to eq 'Looks like your URL is invalid'
    end
  end

  context 'with site without images' do
    before do
      allow_any_instance_of(described_class).to receive(:make_a_request)
        .and_raise(NoImagesError)
    end

    it 'sends a corresponding message' do
      is_expected.to eq "There are no images that can be parsed from html <img src='...'>."
    end
  end

  context 'with site without valid images' do
    before do
      allow_any_instance_of(described_class).to receive(:make_a_request)
        .and_raise(NoValidImagesError)
    end

    it 'sends a corresponding message' do
      is_expected.to eq "Images on this site has invalid extensions. Valid are: #{described_class::VALID_EXTENSIONS}"
    end
  end
end
