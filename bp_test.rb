Dir[File.join(__dir__, 'lib', '*.rb')].each { |file| require file }

module BpTest
  class CLI
    def initialize(argv)
      @app = argv.shift
      @input = argv.shift
    end

    def call
      case @app
      when 'algo'
        array = eval(@input) || [2, 3, 4, 5, 6, 8]
        puts "INPUT:"
        puts "#{array}"
        result = Algo.find_lost_numbers(array)
        puts "OUTPUT:"
        puts result.to_s
      when 'image_parser'
        url = @input || 'http://habr.ru'
        puts "INPUT:"
        puts url
        result = ImageParser.new.call url
        puts "OUTPUT:"
        puts result
      when 'payment_schedule'
        input = if @input
          eval(@input)
        else
          { amount: 10_000, m: 2, n: 10, daily_percent: 5 }
        end
        puts "INPUT:"
        puts "#{input}"
        payment_schedule = PaymentSchedule.new(input)
        puts "OUTPUT:"
        payment_schedule.call
      else
        raise ArgumentError, 'wrong app!'
      end
    end
  end
end

BpTest::CLI.new(ARGV).call
