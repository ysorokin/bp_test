require_relative '../lib/algo'
require 'benchmark/ips'

Benchmark.ips do |x|
  x.report('custom algo') do
    Algo.find_lost_numbers([1, 3, 4, 5, 6, 8])
  end

  x.report('ruby style algo') do
    Algo.find_lost_numbers_ruby_style([1, 3, 4, 5, 6, 8])
  end
end
