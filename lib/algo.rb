#
# https://gist.github.com/sletix/ea94b9e484a833dfd1b2b353763f71b4
#
# У вас есть массив целых чисел. Все числа идут последовательно от 1 до k.
# Но в массиве пропущены 2 числа. Реализуйте алгоритм для нахождения этих чисел.
#
class Algo
  # Если я ничего не путаю, это O(n^2), что наверно не очень оптимально.
  def self.find_lost_numbers(arr, missing_numbers = 2)
    lost_numbers = []

    ideal_size = arr.size + missing_numbers
    (1..ideal_size).each do |i|
      next if arr.include? i
      lost_numbers << i

      break if lost_numbers.size == missing_numbers
    end

    lost_numbers
  end

  # Ruby-style
  def self.find_lost_numbers_ruby_style(arr, missing_numbers = 2)
    ideal_size = arr.size + missing_numbers

    full_array = (1..ideal_size).to_a

    full_array - arr
  end
end
