#
# https://gist.github.com/sletix/ea94b9e484a833dfd1b2b353763f71b4
#
# * Нужно написать программу на любом доступном языке, которая
# получив адрес веб странички, закачает с нее все изображения в папку.
#
require 'httparty'
require 'nokogiri'

class NoImagesError < StandardError; end
class NoValidImagesError < StandardError; end

class ImageParser
  VALID_EXTENSIONS = %w[png jpeg jpg gif svg].freeze

  def call(url = 'http://habr.ru')
    error_wrapper do
      make_a_request(url)
      parse_image_sources
      create_path
      download_images
      'All images downloaded'
    end
  end

  private

  def make_a_request(url)
    @url = URI.parse(url)
    @response = HTTParty.get(@url)
  end

  def parse_image_sources
    doc = Nokogiri::HTML(@response.body)
    @img_urls = doc.xpath('//img/@src').map { |src| URI.join(@url, src).to_s }
    raise NoImagesError if @img_urls.empty?
    @img_urls.select! do |src|
      VALID_EXTENSIONS.any? { |x| src.include?(".#{x}") }
    end
    raise NoValidImagesError if @img_urls.empty?
  end

  def create_path
    @dir_name = "#{@url.host}_images"
    Dir.mkdir @dir_name unless File.exist?(@dir_name)
  end

  def download_images
    @img_urls.each do |uri|
      begin
        file_path = [@dir_name, File.basename(uri)].join('/')

        File.open(file_path, 'wb') do |f|
          get_image = HTTParty.get(uri)
          f.write(get_image.body)
        end
      rescue StandardError => _e
        puts "Image #{uri} not able to download"
        next
      end
    end
  end

  def error_wrapper
    yield
  rescue Errno::ECONNREFUSED
    'Looks like your URL is invalid'
  rescue NoImagesError
    "There are no images that can be parsed from html <img src='...'>."
  rescue NoValidImagesError
    "Images on this site has invalid extensions. Valid are: #{VALID_EXTENSIONS}"
  end
end
