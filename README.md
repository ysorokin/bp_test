# BP TEST

Задание: https://gist.github.com/sletix/ea94b9e484a833dfd1b2b353763f71b4

По всем задачам можно запускать без последнего аргумента, тогда скрипт запустится с дефолтным значением.

Перед выполнением надо установить гемы: `bundle install`

## Задача 1: Image Parser

```sh
ruby bp_test.rb image_parse 'http://habr.ru'
```

## Задача 2: Algo

Не уверен, что приведённые алгоритмы оптимальные, но работают как должны.

```sh
ruby bp_test.rb algo '[1, 3, 4, 5, 6, 8, 9]'
```

## Задача 3: SQL QUERY

см. в `lib/sql_query.sql`

## Задача 4: Payment Schedule

```sh
ruby bp_test.rb payment_schedule '{ amount: 10_000, m: 4, n: 20, daily_percent: 5 }'
```
