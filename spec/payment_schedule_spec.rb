require 'payment_schedule'

RSpec.describe PaymentSchedule do
  let(:amount) { 1_002 }
  let(:m) { 4 }
  let(:n) { 20 }
  let(:daily_percent) { 20 }

  subject do
    described_class.new(amount: amount, m: m, n: n, daily_percent: daily_percent)
  end

  describe 'validate input' do
    context 'with valid input' do
      it { expect { subject }.not_to raise_error }
    end

    context 'invalid m and n' do
      let(:m) { 7 }

      it { expect { subject }.to raise_error(ArgumentError) }
    end

    context 'invalid percent' do
      let(:daily_percent) { 1.32 }

      it { expect { subject }.to raise_error(ArgumentError) }
    end

    context 'innvalid amount' do
      let(:amount) { -100_000 }

      it { expect { subject }.to raise_error(ArgumentError) }
    end
  end

  describe 'calculations' do
    it 'all correct' do
      expect(subject.percent).to eq 400
      expect(subject.percent_amount).to eq 4_000
      expect(subject.total_debt).to eq 5_002
      expect(subject.period_payment).to eq 1_250
      expect(subject.total_remainder).to eq 2
      expect(subject.payments_array).to eq [1251, 1251, 1250, 1250]
    end
  end

  describe 'call' do
    # TODO: test common output
  end
end
