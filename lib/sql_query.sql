/*
https://gist.github.com/sletix/ea94b9e484a833dfd1b2b353763f71b4

Есть таблица Users(id, email), есть таблица Messages(id, user_id, message). 
Нужно написать sql запрос который вернет 10 пользователей с максимальным кол-ом сообщений
*/ 

-- Вариант 1
SELECT users.id, email, COALESCE(number_of_messages, 0) AS number_of_messages 
FROM users LEFT JOIN (SELECT user_id, COUNT(id) AS number_of_messages
FROM messages
GROUP BY user_id) grouped_messages
ON users.id = grouped_messages.user_id
ORDER BY number_of_messages DESC nulls last
LIMIT 10;